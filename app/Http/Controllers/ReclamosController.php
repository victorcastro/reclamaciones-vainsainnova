<?php

namespace App\Http\Controllers;

use App\Reclamos;
use App\Tienda;
use App\TipoBien;
use App\TipoComprobante;
use App\TipoDocumento;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Session;
use Exception;

class ReclamosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['create', 'ajxCrearCodigoReclamo', 'ajxRegistrarReclamo']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $inputFrom = $request->input('from');
        $inputTo = $request->input('to');

        $from = $inputFrom ? $inputFrom.' 00:00:00' : Carbon::now()->format('Y-m-d 00:00:00');
        $to = $inputTo ? $inputTo.' 23:59:59' : Carbon::now()->format('Y-m-d 23:59:59');

        $reclamos = Reclamos::whereBetween('reclamoFecha', [$from, $to])->orderBy('reclamoFecha', 'DESC')->paginate(6);

        $viewDateFrom = Carbon::createFromDate($from)->format('l d/m/Y');
        $viewDateTo = Carbon::createFromDate($to)->format('l d/m/Y');

        return view('reclamos.index', compact('reclamos', 'viewDateFrom', 'viewDateTo', 'inputFrom', 'inputTo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lstTiendas = Tienda::all();
        $lstTipoBien = TipoBien::all();
        $lstTipoComprobante = TipoComprobante::all();
        $lstTipoDocumento = TipoDocumento::all();

        return view('reclamos.create', compact('lstTiendas', 'lstTipoBien', 'lstTipoComprobante', 'lstTipoDocumento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->tienda);
        $reclamo = [
            'codigoTienda' => $request->tienda,
        ];
        /*
        $reclamo["fecha_reclamo"] = $request->input('fecha_reclamo');
    	$reclamo["num_carta_reclamo"] = $request->input('num_carta_reclamo');
    	$reclamo["tipo_bien"] = $request->input('tipo_bien');
    	$reclamo["tienda"] = $request->input('tienda');
    	$reclamo["nombre_cliente"] = $request->input('nombre_cliente');
        $reclamo['appat_cliente'] = $request->input('appat_cliente');
    	$reclamo['apmat_cliente'] = $request->input('apmat_cliente');
    	$reclamo["direccion_cliente"] = $request->input('direccion_cliente');
    	$reclamo["codigo_departamento"] = $request->input('codigo_departamento');
    	$reclamo["codigo_provincia"] = $request->input('codigo_provincia');
    	$reclamo["codigo_distrito"] = $request->input('codigo_distrito');
    	$reclamo["tipo_doc"] = $request->input('tipo_doc');
    	$reclamo["dni_cliente"] = $request->input('dni_cliente');
    	$reclamo["telefono_cliente"] = $request->input('telefono_cliente');
    	$reclamo["mail_cliente"] = $request->input('mail_cliente');
    	$reclamo["es_mayor_edad"] = $request->input('es_mayor_edad');
    	$reclamo["codigo_producto"] = $request->input('codigo_producto');
    	$reclamo["tipo_comprobante"] = $request->input('tipo_comprobante');
    	$reclamo["serie_comprobante"] = $request->input('serie_comprobante');
    	$reclamo["numero_comprobante"] = $request->input('numero_comprobante');
    	$reclamo["fecha_factura"] = $request->input('fecha_factura');
    	$reclamo["detalle"] = $request->input('detalle');
    	$reclamo["apoderado_cliente"] = $request->input('apoderado_cliente');
    	$reclamo["metodo_notificacion"] = $request->input('metodo_notificacion');
    	$reclamo["precio_producto"] = $request->input('precio_producto');
    	$reclamo["descripcion_producto"] = $request->input('descripcion_producto');
    	$reclamo["reclamo_procesado"] = $request->input('reclamo_procesado');
    	$reclamo["reclamo_codigo_externo"] = $request->input('reclamo_codigo_externo');
        */
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reclamos  $reclamos
     * @return \Illuminate\Http\Response
     */
    public function show(Reclamos $reclamo)
    {
        return view('reclamos.show', compact('reclamo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reclamos  $reclamos
     * @return \Illuminate\Http\Response
     */
    public function edit(Reclamos $reclamos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reclamos  $reclamos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reclamos $reclamos)
    {
        //
    }

    public function ajxCrearCodigoReclamo(Request $request)
    {
        $numeroInicio = 394;
        $cantReclamos = Reclamos::count();
        $codReclamo = "LR01-".str_pad(($numeroInicio+$cantReclamos+1), 6, "0", STR_PAD_LEFT);
        $arrResponse = array('codigoReclamo' => $codReclamo);

        return json_encode($arrResponse);
    }

    public function ajxRegistrarReclamo(Request $request)
    {

        $idReclamo = "";

        try {

            $reclamo = array();

            $reclamo["fecha_reclamo"] = $request->input('fecha_reclamo');
            $reclamo["num_carta_reclamo"] = $request->input('num_carta_reclamo');
            $reclamo["tipo_bien"] = $request->input('tipo_bien');
            $reclamo["tienda"] = $request->input('tienda');
            $reclamo["nombre_cliente"] = $request->input('nombre_cliente');
            $reclamo['appat_cliente'] = $request->input('appat_cliente');
            $reclamo['apmat_cliente'] = $request->input('apmat_cliente');
            $reclamo["direccion_cliente"] = $request->input('direccion_cliente');
            $reclamo["codigo_departamento"] = $request->input('codigo_departamento');
            $reclamo["codigo_provincia"] = $request->input('codigo_provincia');
            $reclamo["codigo_distrito"] = $request->input('codigo_distrito');
            $reclamo["tipo_doc"] = $request->input('tipo_doc');
            $reclamo["dni_cliente"] = $request->input('dni_cliente');
            $reclamo["telefono_cliente"] = $request->input('telefono_cliente');
            $reclamo["mail_cliente"] = $request->input('mail_cliente');
            $reclamo["es_mayor_edad"] = $request->input('es_mayor_edad');
            $reclamo["codigo_producto"] = $request->input('codigo_producto');
            $reclamo["tipo_comprobante"] = $request->input('tipo_comprobante');
            $reclamo["serie_comprobante"] = $request->input('serie_comprobante');
            $reclamo["numero_comprobante"] = $request->input('numero_comprobante');
            $reclamo["fecha_factura"] = $request->input('fecha_factura');
            $reclamo["detalle"] = $request->input('detalle');
            $reclamo["apoderado_cliente"] = $request->input('apoderado_cliente');
            $reclamo["metodo_notificacion"] = $request->input('metodo_notificacion');
            $reclamo["precio_producto"] = $request->input('precio_producto');
            $reclamo["descripcion_producto"] = $request->input('descripcion_producto');
            $reclamo["reclamo_procesado"] = $request->input('reclamo_procesado');
            $reclamo["reclamo_codigo_externo"] = $request->input('reclamo_codigo_externo');

            $idReclamo = Reclamos::registrarReclamo($reclamo);

            $reclamo["id_reclamo"] = $idReclamo;

            Session::put('objReclamo', $reclamo);

        } catch(Exception $ex){
            $exceptionMessage = $ex->getMessage();

        }

        $arrResponse = array('idReclamo' => $idReclamo);

        return json_encode($arrResponse);
    }

}
