<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;


class CaptchaController extends Controller {

    public function getCaptcha(Request $request){

        if($request->ajax()) {
            $result="";
            $rules = array('captcha' => ['required', 'captcha']);
            $validator = Validator::make(
                ['captcha' => $request->input('captcha')],
                $rules,
                // Mensaje de error personalizado
                ['captcha' => 'El captcha ingresado es incorrecto.']
            );
            if ($validator->passes()) {
                //echo "Todo Correcto, sigue codeando :)";
                $result="exito";
            } else {
                $result="error";
            }

            $arrResponse = array('result' => $result);
            return json_encode($arrResponse);
        }
    }

    public function refreshCapcha(){
        return captcha_img('flat');
    }
}
