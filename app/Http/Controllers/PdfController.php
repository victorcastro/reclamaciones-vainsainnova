<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

use App\Http\Requests;

class PdfController extends Controller
{
   	public function invoice() 
    {
        $pdf = PDF::loadView('pdf.invoice');
        return $pdf->download('invoice.pdf');
    }


}
