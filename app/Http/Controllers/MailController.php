<?php
namespace App\Http\Controllers;
use App\Reclamos;
use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Mail;

use PDF;
use Session;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class MailController extends Controller {

    public function enviarEmail(Request $request)
    {
        $data = Session::get('objReclamo');
        $user = User::getAdmin()->toArray();
        $idReclamo = $data["id_reclamo"];

        $fromemail = "mundosole@sole.com.pe";
        $toEmail = $data["mail_cliente"];
        $ccoEmail = $user["email"];
        $numReclamo = $data["num_carta_reclamo"];
        $nombreCliente = $data["nombre_cliente"]." ".$data["appat_cliente"]." ".$data["apmat_cliente"];

        $data = array(  'fromemail' => $fromemail,
                        'toemail' => $toEmail,
                        'ccoemail' => $ccoEmail,
                        'numreclamo' => $numReclamo,
                        'nombrecliente' => $nombreCliente
                );

        $result = array('numreclamo' => $numReclamo,
                        'nombrecliente' => $nombreCliente,
                        'idReclamo' => $idReclamo
                  );

        $reclamo = Reclamos::obtenerReclamo($idReclamo);

        $string = file_get_contents("http://reclamaciones.sole.com.pe/js/ubigeo-peru.min.json");
        $json_a = json_decode($string, true);
        $dpto_text = array();

        foreach($json_a as $item){
            if($item["departamento"] == $reclamo->clienteDepartamento && $item["provincia"] == "00" && $item["distrito"] == "00"){
                $dpto_text["departamento"] = $item["nombre"];
            }

            if($item["departamento"] == $reclamo->clienteDepartamento && $item["provincia"] == $reclamo->clienteProvincia && $item["distrito"] == "00"){
                $dpto_text["provincia"] = $item["nombre"];
            }

            if($item["departamento"] == $reclamo->clienteDepartamento && $item["provincia"] == $reclamo->clienteProvincia && $item["distrito"] == $reclamo->clienteDistrito){
                $dpto_text["distrito"] = $item["nombre"];
            }
        }

        $data2 = array(
            'reclamo' => $reclamo,
            'ubigeo' => $dpto_text
        );

        $find = Setting::where('key', 'NOTIFICATION_EMAILS_ADMIN')->pluck('value')->first();
        $emailNotificacions = explode(',', $find);

        view()->share('data', $data2);
        $pdf = PDF::loadView('pdf.invoice');

        if($toEmail != ""){
            Mail::send('email.send', $data, function($message) use($data, $pdf, $emailNotificacions) {
                $message
                    ->to($data['toemail'])
                    // ->cc($data['ccoemail'])
                    ->bcc($emailNotificacions)
                    ->from($data['fromemail'])
                    ->subject('Registro de Reclamo: '.$data['numreclamo'].' - www.sole.com.pe');
                $message->attachData($pdf->output(), $data['numreclamo'].".pdf");
            });
        }

        if(count(Mail::failures()) > 0){
            $errors = 'Failed to send password reset email, please try again.';
        }

        Session::flush();

        return view('regreclamo')->with('resultados', $result);

    }



}
