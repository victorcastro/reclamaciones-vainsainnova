<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Spatie\Permission\Models\Role;

;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name');

        return view('user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User([
            'name' => $request->name,
            'first_lastname'=> $request->first_lastname,
            'second_lastname'=> $request->second_lastname,
            'email'=> $request->email,
            'mobile'=> $request->mobile,
            'password'=> bcrypt($request->password),
        ]);

        $user->save();
        $user->assignRole($request->role);
        return Redirect::route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return Redirect::route('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $success = false;

        return view('user.edit', compact('user', 'success'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $success = false;
        $request->validate([
            'name' => 'required|min:2|max:25',
            'first_lastname' => 'required|max:20',
            'second_lastname' => 'required|max:20',
            'email' => 'required|email|max:35',
            'mobile' => 'required|max:15',
        ]);

        $user = User::findOrFail($id);
        $user->name = $request->input('name');
        $user->first_lastname = $request->input('first_lastname');
        $user->second_lastname = $request->input('second_lastname');
        $user->email = $request->input('email');
        $user->mobile = $request->input('mobile');

        if (!empty($request->input('password')))
            $user->password = bcrypt($request->input('password'));

        if ($user->save()) {
            $success = 'Guardado correctamente';
        }

        return view('user.edit', compact('user', 'success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return Redirect::route('user.index');
    }
}
