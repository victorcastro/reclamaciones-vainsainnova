<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_lastname', 'second_lastname', 'email', 'mobile', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function scopeValidarUsuario($query, $usuario, $clave)
    {	return $query->where('usuario', '=', $usuario)
        ->where('clave', '=', $clave)->get();

    }

    public function scopeInfoUsuario($query, $idUsuario)
    {	return $query->where('id', '=', $idUsuario)->get();  }

    public function scopeGetAdmin($query)
    {
        return $query->where('email', 'haraoz@sole.com.pe')->first();
    }

    public function scopeActualizarUsuario($query, $usuario)
    {
        $updateResult = $query->where('id', '=', $usuario["idUsuario"])
            ->update(array('usuario' => $usuario["usuario"],
                'clave' => $usuario["clave"],
                'nombre' => $usuario["nombre"],
                'email' => $usuario["email"]
            ));

        if($updateResult=="1")
        {   return true;    }

        return false;

    }
}
