<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reclamos extends Model
{
    public $table = 'reclamo';

    protected $primaryKey = 'idReclamo';

    protected $dates = ['reclamoFecha'];

    protected $fillable = [
        'codigoTienda','codigoTipoBien', 'codigoTipoDocumento', 'codigoTipoComprobante', 'reclamoNumero', 'reclamoDetalle', 'reclamoFecha', 'clienteNombre', 'clienteAppaterno', 'clienteApmaterno',
        'clienteNumDoc', 'clienteEmail', 'clienteDireccion', 'clienteDepartamento', 'clienteProvincia', 'clienteDistrito', 'clienteTelefono', 'clienteEsMayorEdad', 'clienteApoderado',
        'clienteMetodoNotificacion', 'comprobanteFecha', 'comprobanteSerie', 'comprobanteNumero', 'productoCodigo', 'productoPrecio', 'productoDescripcion', 'flagProcesado'
    ];

    public $attributes = [ 'nombre_completo_cliente', 'datefrom' ];

    public function getNombreCompletoClienteAttribute($value)
    {
        return strtoupper("{$this->clienteNombre}  {$this->clienteAppaterno} {$this->clienteApmaterno}");
    }

    public function getDateFromAttribute()
    {
        $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $this->reclamoFecha);
        $sinceHours = $createdAt->diffInHours();
        $sinceMinutes = $createdAt->diffInMinutes();

        if ($sinceHours === 1)
            return 'Hace '.$sinceHours.' hora';
        else if ($sinceHours < 1)
            return 'Hace '.$sinceMinutes.' minutos';
        else if ($sinceHours < 23)
            return 'Hace '.$sinceHours.' horas';
        else
            return $createdAt->format('l d/m/Y');
    }

    public function tienda() {
        return $this->belongsTo(Tienda::class, 'codigoTienda', 'codigoTienda');
    }

    public function tipoBien() {
        return $this->belongsTo(TipoBien::class, 'codigoTipoBien', 'codigoTipoBien');
    }

    public function tipoComprobante() {
        return $this->belongsTo(TipoComprobante::class, 'codigoTipoComprobante', 'codigoTipoComprobante');
    }

    public function tipoDocumento() {
        return $this->belongsTo(TipoDocumento::class, 'codigoTipoDocumento', 'codigoTipoDocumento');
    }

    public function scopeObtenerReclamo($query, $idReclamo)
    {
        return $query->where('idReclamo',$idReclamo)->get()->first();

    }

    public function scopeRegistrarReclamo($query, $reclamo)
    {
        //'reclamo_codigo_externo': jsn_reclamo_codigo_externo

        $fechaReclamo = $reclamo['fecha_reclamo'];
        $fechaFactura = $reclamo['fecha_factura'];
        $dataReclamo = array(
            'codigoTienda'			=> $reclamo['tienda'],
            'codigoTipoBien'		=> $reclamo['tipo_bien'],
            'codigoTipoDocumento'	=> $reclamo['tipo_doc'],
            'codigoTipoComprobante'	=> $reclamo['tipo_comprobante'],
            'reclamoNumero'			=> $reclamo['num_carta_reclamo'],
            'reclamoDetalle'		=> $reclamo['detalle'],
            'reclamoFecha'			=> DB::raw("STR_TO_DATE('$fechaReclamo','%d/%m/%Y')"),
            'clienteNombre'			=> $reclamo['nombre_cliente'],
            'clienteAppaterno'		=> $reclamo['appat_cliente'],
            'clienteApmaterno'		=> $reclamo['apmat_cliente'],
            'clienteNumDoc'			=> $reclamo['dni_cliente'],
            'clienteEmail'			=> $reclamo['mail_cliente'],
            'clienteDireccion'		=> $reclamo['direccion_cliente'],
            'clienteDepartamento'   => $reclamo['codigo_departamento'],
            'clienteProvincia'   	=> $reclamo['codigo_provincia'],
            'clienteDistrito'   	=> $reclamo['codigo_distrito'],
            'clienteTelefono'		=> $reclamo['telefono_cliente'],
            'clienteEsMayorEdad'	=> $reclamo['es_mayor_edad'],
            'clienteApoderado'		=> $reclamo['apoderado_cliente'],
            'clienteMetodoNotificacion'   => $reclamo['metodo_notificacion'],
            'comprobanteFecha'		=> DB::raw("STR_TO_DATE('$fechaFactura','%d/%m/%Y')"),
            'comprobanteSerie'		=> $reclamo['serie_comprobante'],
            'comprobanteNumero'		=> $reclamo['numero_comprobante'],
            'productoCodigo'		=> $reclamo['codigo_producto'],
            'productoPrecio'		=> $reclamo['precio_producto'],
            'productoDescripcion'	=> $reclamo['descripcion_producto'],
            'flagProcesado'			=> $reclamo['reclamo_procesado']
        );

        $idReclamo = $query->insertGetId($dataReclamo);

        return $idReclamo;

    }
}
