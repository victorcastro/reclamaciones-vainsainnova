<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'ReclamosController@index')->name('home');
Route::resource('user','UserController');
Route::resource('reclamos', 'ReclamosController');
Route::resource('configuraciones', 'SettingController');

Route::get('nuevo', 'ReclamosController@create');

Route::get('/ajax/crearcodigo', 'ReclamosController@ajxCrearCodigoReclamo');
Route::post('/ajax/registrar', 'ReclamosController@ajxRegistrarReclamo');
// Route::get('/ajax/inforeclamo', 'ReclamosController@ajxInfoReclamo');
// Route::get('/ajax/actualizarflagreclamo', 'ReclamosController@ajxActualizarFlagReclamo');

Route::get('/enviarmail','MailController@enviarEmail');

Route::any('/getcaptcha', 'CaptchaController@getCaptcha' );
Route::get('/refreshcapcha', 'CaptchaController@refreshCapcha');
