// ==============
// LLENADO UBIGEO
// ==============

var ubigeoPeru = {
    ubigeos: new Array()
};



document.addEventListener('DOMContentLoaded', function() {
    var request = new XMLHttpRequest;
    request.open('GET', '/js/ubigeo-peru.min.json', true);
    request.onload = onLoad_Request;
    request.send();

    function onLoad_Request() {
        if (request.status >= 200 && request.status < 400) {
            ubigeoPeru.ubigeos = JSON.parse(request.responseText);
            showRegionsList();
        }
    }
});



function showRegionsList() {
    var optReg = document.createElement('option');
    optReg.id = "";
    optReg.value = "";
    optReg.textContent = "Seleccione un departamento";
    document.querySelector('#dptoDir').appendChild(optReg);

    ubigeoPeru.ubigeos.forEach(function(ubigeo) {
        if (ubigeo.provincia === '00' && ubigeo.distrito === '00') {
            var option = document.createElement('option');
            option.id = 'dpto-' + ubigeo.departamento;
            option.value = ubigeo.departamento;
            option.textContent = ubigeo.nombre;
            document.querySelector('#dptoDir').addEventListener('change', onChange_Region, false);
            document.querySelector('#dptoDir').appendChild(option);
        }
    });
}



function onChange_Region() {
    document.querySelector('#provDir').innerHTML = '';
    document.querySelector('#distDir').innerHTML = '';
    showProvincesList(this.value);
}



function showProvincesList(departamento) {
    var optProv = document.createElement('option');
    optProv.id = "";
    optProv.value = "";
    optProv.textContent = "Seleccione una Provincia";
    document.querySelector('#provDir').appendChild(optProv);

    ubigeoPeru.ubigeos.forEach(function(ubigeo) {
        if (ubigeo.departamento === departamento && ubigeo.provincia !== '00' && ubigeo.distrito === '00') {
            var option = document.createElement('option');
            option.id = 'prov-' + ubigeo.provincia;
            option.value = ubigeo.provincia;
            option.textContent = ubigeo.nombre;
            document.querySelector('#provDir').addEventListener('change', onChange_Province, false);
            document.querySelector('#provDir').appendChild(option);
        }
    });

}



function onChange_Province() {
    document.querySelector('#distDir').innerHTML = '';
    var departamento = document.querySelector('#dptoDir').value;
    showDistrictsList(departamento, this.value);
}

function showDistrictsList(departamento, provincia) {
    var optDist = document.createElement('option');
    optDist.id = "";
    optDist.value = "";
    optDist.textContent = "Seleccione un Distrito";
    document.querySelector('#distDir').appendChild(optDist);

    ubigeoPeru.ubigeos.forEach(function(ubigeo) {
        if (ubigeo.departamento === departamento && ubigeo.provincia === provincia && ubigeo.distrito !== '00') {
            var option = document.createElement('option');
            option.id = 'prov-' + ubigeo.distrito;
            option.value = ubigeo.distrito;
            option.textContent = ubigeo.nombre;
            document.querySelector('#distDir').appendChild(option);
        }
    });

}

// ==============
// ENVIO DE DATOS
// ==============

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}


$("#btn_enviar").click(function(e) {
    const googleResponse = jQuery('#g-recaptcha-response').val();

    if (!googleResponse) {
        e.preventDefault();
        $("#captcha").removeClass("captcha-textbox").addClass("captcha-textbox-error");
        $("#msg_error_captcha").html("<strong>ERROR:</strong> Por favor realice la validación del captcha").show();
    } else {
        $("#captcha").removeClass("captcha-textbox-error").addClass("captcha-textbox");
        $("#msg_error_captcha").html("");

        var str_rec_dia = $("#reclamoFecha_dia").val();
        var str_rec_mes = $("#reclamoFecha_mes").val();
        var str_rec_anho = $("#reclamoFecha_anho").val();
        var str_doc_dia = $("#docFecha_dia").val();
        var str_doc_mes = $("#docFecha_mes").val();
        var str_doc_anho = $("#docFecha_anho").val();
        var str_tienda = $("#tienda").val();
        var str_tipo_doc = $("#tipoDocumento").val();
        var str_es_mayor = $("#clienteEsMayorEdad").val();
        var str_tipo_bien = $("#tipoBien").val();
        var str_tipo_comp = $("#tipoComprobante").val();
        var str_cod_dpto = $("#dptoDir").val();
        var str_cod_prov = $("#provDir").val();
        var str_cod_dist = $("#distDir").val();
        var str_met_notif = $("#tipoComuReclamo").val();

        // TEXTBOX
        var str_reclamonumero = $("#reclamoNumero").val();
        var str_clienteNombre = $("#clienteNombre").val();
        var str_clienteAppaterno = $("#clienteAppaterno").val();
        var str_clienteApmaterno = $("#clienteApmaterno").val();
        var str_clienteNumDoc = $("#clienteNumDoc").val();
        var str_clienteEmail = $("#clienteEmail").val();
        var str_clienteDireccion = $("#clienteDireccion").val();
        var str_clienteTelefono = $("#clienteTelefono").val();
        var str_clienteApoderado = $("#clienteApoderado").val();
        var str_comprobanteSerie = $("#comprobanteSerie").val();
        var str_comprobanteNumero = $("#comprobanteNumero").val();
        var str_productoCodigo = $("#productoCodigo").val();
        var str_productoPrecio = $("#productoPrecio").val();
        var str_productoDescripcion = $("#productoDescripcion").val();
        var str_reclamoDetalle = $("#reclamoDetalle").val();

        var errorcount = 0;
        // ==================
        // TEXTBOX VALIDATION
        // ==================
        if (str_reclamonumero=="") {
            $("#reclamoNumero_title").removeClass("form-label").addClass("form-label-error");
            $("#reclamoNumero_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#reclamoNumero_title").removeClass("form-label-error").addClass("form-label");
            $("#reclamoNumero_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_clienteNombre=="") {
            $("#clienteNombre_title").removeClass("form-label").addClass("form-label-error");
            $("#clienteNombre_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#clienteNombre_title").removeClass("form-label-error").addClass("form-label");
            $("#clienteNombre_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_clienteAppaterno=="") {
            $("#clienteAppaterno_title").removeClass("form-label").addClass("form-label-error");
            $("#clienteAppaterno_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#clienteAppaterno_title").removeClass("form-label-error").addClass("form-label");
            $("#clienteAppaterno_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_clienteApmaterno=="") {
            $("#clienteApmaterno_title").removeClass("form-label").addClass("form-label-error");
            $("#clienteApmaterno_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#clienteApmaterno_title").removeClass("form-label-error").addClass("form-label");
            $("#clienteApmaterno_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_clienteNumDoc=="") {
            $("#clienteNumDoc_title").removeClass("form-label").addClass("form-label-error");
            $("#clienteNumDoc_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#clienteNumDoc_title").removeClass("form-label-error").addClass("form-label");
            $("#clienteNumDoc_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_clienteDireccion=="") {
            $("#clienteDireccion_title").removeClass("form-label").addClass("form-label-error");
            $("#clienteDireccion_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#clienteDireccion_title").removeClass("form-label-error").addClass("form-label");
            $("#clienteDireccion_control").removeClass("form-control-error").addClass("form-control");
        }



        if(str_clienteTelefono=="") {
            $("#clienteTelefono_title").removeClass("form-label").addClass("form-label-error");
            $("#clienteTelefono_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#clienteTelefono_title").removeClass("form-label-error").addClass("form-label");
            $("#clienteTelefono_control").removeClass("form-control-error").addClass("form-control");
        }



        if(str_comprobanteSerie=="") {
            $("#comprobanteSerie_title").removeClass("form-label").addClass("form-label-error");
            $("#comprobanteSerie_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#comprobanteSerie_title").removeClass("form-label-error").addClass("form-label");
            $("#comprobanteSerie_control").removeClass("form-control-error").addClass("form-control");
        }



        if(str_comprobanteNumero=="") {
            $("#comprobanteNumero_title").removeClass("form-label").addClass("form-label-error");
            $("#comprobanteNumero_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#comprobanteNumero_title").removeClass("form-label-error").addClass("form-label");
            $("#comprobanteNumero_control").removeClass("form-control-error").addClass("form-control");
        }



        if(str_productoCodigo=="") {
            $("#productoCodigo_title").removeClass("form-label").addClass("form-label-error");
            $("#productoCodigo_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#productoCodigo_title").removeClass("form-label-error").addClass("form-label");
            $("#productoCodigo_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_productoPrecio=="") {
            $("#productoPrecio_title").removeClass("form-label").addClass("form-label-error");
            $("#productoPrecio_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#productoPrecio_title").removeClass("form-label-error").addClass("form-label");
            $("#productoPrecio_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_productoDescripcion=="") {
            $("#productoDescripcion_title").removeClass("form-label").addClass("form-label-error");
            $("#productoDescripcion_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#productoDescripcion_title").removeClass("form-label-error").addClass("form-label");
            $("#productoDescripcion_control").removeClass("form-control-error").addClass("form-control");
        }


        if (str_reclamoDetalle=="") {
            $("#reclamoDetalle_title").removeClass("form-label").addClass("form-label-error");
            $("#reclamoDetalle_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#reclamoDetalle_title").removeClass("form-label-error").addClass("form-label");
            $("#reclamoDetalle_control").removeClass("form-control-error").addClass("form-control");
        }

        // ==================
        // 	SELECT VALIDATION
        // ==================
        if (str_rec_dia=="-") {
            $("#reclamoFecha_title").removeClass("form-label").addClass("form-label-error");
            $("#reclamoFecha_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#reclamoFecha_title").removeClass("form-label-error").addClass("form-label");
            $("#reclamoFecha_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_rec_mes=="-") {
            $("#reclamoFecha_title").removeClass("form-label").addClass("form-label-error");
            $("#reclamoFecha_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#reclamoFecha_title").removeClass("form-label-error").addClass("form-label");
            $("#reclamoFecha_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_rec_anho=="-") {
            $("#reclamoFecha_title").removeClass("form-label").addClass("form-label-error");
            $("#reclamoFecha_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#reclamoFecha_title").removeClass("form-label-error").addClass("form-label");
            $("#reclamoFecha_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_doc_dia=="-") {
            $("#docFecha_title").removeClass("form-label").addClass("form-label-error");
            $("#docFecha_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#docFecha_title").removeClass("form-label-error").addClass("form-label");
            $("#docFecha_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_doc_mes=="-") {
            $("#docFecha_title").removeClass("form-label").addClass("form-label-error");
            $("#docFecha_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#docFecha_title").removeClass("form-label-error").addClass("form-label");
            $("#docFecha_control").removeClass("form-control-error").addClass("form-control");
        }

        if (str_doc_anho=="-") {
            $("#docFecha_title").removeClass("form-label").addClass("form-label-error");
            $("#docFecha_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#docFecha_title").removeClass("form-label-error").addClass("form-label");
            $("#docFecha_control").removeClass("form-control-error").addClass("form-control");
        }


        if (str_es_mayor=="N") {
            if (str_clienteApoderado=="") {
                $("#clienteApoderado_title").removeClass("form-label").addClass("form-label-error");
                $("#clienteApoderado_control").removeClass("form-control").addClass("form-control-error");
                errorcount = errorcount + 1;
            } else {
                $("#clienteApoderado_title").removeClass("form-label-error").addClass("form-label");
                $("#clienteApoderado_control").removeClass("form-control-error").addClass("form-control");
            }
        } else {
            $("#clienteEsMayorEdad_title").removeClass("form-label-error").addClass("form-label");
            $("#clienteEsMayorEdad_control").removeClass("form-control-error").addClass("form-control");
        }

        if($("#dptoDir option").size() > 0){
            if(str_cod_dpto == ""){
                $("#dptoDir_title").removeClass("form-label").addClass("form-label-error");
                $("#dptoDir_control").removeClass("form-control").addClass("form-control-error");
                errorcount = errorcount + 1;
            }else{
                $("#dptoDir_title").removeClass("form-label-error").addClass("form-label");
                $("#dptoDir_control").removeClass("form-control-error").addClass("form-control");
            }
        }else{
            $("#dptoDir_title").removeClass("form-label").addClass("form-label-error");
            $("#dptoDir_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        }

        if($("#provDir option").size() > 0){
            if(str_cod_prov == "" || str_cod_prov === undefined){
                $("#provDir_title").removeClass("form-label").addClass("form-label-error");
                $("#provDir_control").removeClass("form-control").addClass("form-control-error");
                errorcount = errorcount + 1;
            }else{
                $("#provDir_title").removeClass("form-label-error").addClass("form-label");
                $("#provDir_control").removeClass("form-control-error").addClass("form-control");
            }
        }else{
            $("#provDir_title").removeClass("form-label").addClass("form-label-error");
            $("#provDir_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        }

        if ($("#distDir option").size() > 0){
            if(str_cod_dist == "" || str_cod_dist === undefined){
                $("#distDir_title").removeClass("form-label").addClass("form-label-error");
                $("#distDir_control").removeClass("form-control").addClass("form-control-error");
                errorcount = errorcount + 1;
            }else{
                $("#distDir_title").removeClass("form-label-error").addClass("form-label");
                $("#distDir_control").removeClass("form-control-error").addClass("form-control");
            }
        } else {
            $("#distDir_title").removeClass("form-label").addClass("form-label-error");
            $("#distDir_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        }

        if (str_met_notif == ""){
            $("#tipoComuReclamo_title").removeClass("form-label").addClass("form-label-error");
            $("#tipoComuReclamo_control").removeClass("form-control").addClass("form-control-error");
            errorcount = errorcount + 1;
        } else {
            $("#tipoComuReclamo_title").removeClass("form-label-error").addClass("form-label");
            $("#tipoComuReclamo_control").removeClass("form-control-error").addClass("form-control");
        }


        // ==================
        // EMAIL VALIDATION
        // ==================

        const attrEmail = $("#clienteEmail").attr('required');
        if (typeof attrEmail !== typeof undefined && attrEmail !== false) {
            if (!isValidEmailAddress(str_clienteEmail) || str_clienteEmail == ""){
                $("#clienteEmail_title").removeClass("form-label").addClass("form-label-error");
                $("#clienteEmail_control").removeClass("form-control").addClass("form-control-error");
                errorcount = errorcount + 1;
            } else {
                $("#clienteEmail_title").removeClass("form-label-error").addClass("form-label");
                $("#clienteEmail_control").removeClass("form-control-error").addClass("form-control");
            }
        }

        if (errorcount>0) {
            $("#msg_error").html("<strong>ERROR:</strong> Por favor revise el formato/contenido de la información ingresada").show();
        } else {
            $("#msg_error").html("");
            $("#btn_enviar").hide();
            $("#captcha-box").hide();
            $("#msg_enviar").show();


            // ==================================================================
            // IMPORTANTE (no se estan considerando estos campos al envio del WS)
            // ==================================================================
            var jsn_apoderado_cliente = str_clienteApoderado;
            var jsn_precio_producto = str_productoPrecio;
            var jsn_descripcion_producto = str_productoDescripcion;

            // ==================================================================
            var jsn_fecha_reclamo=str_rec_dia+"/"+str_rec_mes+"/"+str_rec_anho
            var jsn_tipo_bien=str_tipo_bien;
            var jsn_tienda=str_tienda;
            var jsn_nombre_cliente=str_clienteNombre+" "+str_clienteAppaterno+" "+str_clienteApmaterno;
            var jsn_direccion_cliente=str_clienteDireccion;
            var jsn_tipo_doc=str_tipo_doc;
            var jsn_dni_cliente=str_clienteNumDoc;
            var jsn_telefono_cliente=str_clienteTelefono;
            var jsn_mail_cliente=str_clienteEmail;
            var jsn_apoderado=str_es_mayor;
            var jsn_codigo_producto=str_productoCodigo;
            var jsn_tipo_comprobante=str_tipo_comp;
            var jsn_serie_comprobante=str_comprobanteSerie;
            var jsn_numero_comprobante=str_comprobanteNumero;
            var jsn_fecha_factura=str_doc_dia+"/"+str_doc_mes+"/"+$("#docFecha_anho").val();
            var jsn_detalle=str_reclamoDetalle;
            var jsn_cod_dpto = str_cod_dpto;
            var jsn_cod_prov = str_cod_prov;
            var jsn_cod_dist = str_cod_dist;
            var jsn_met_notif = str_met_notif;

            // Codigo de Reclamo Autogenerado por Laravel
            var jsn_num_carta_reclamo="";

            // =================================================================
            // 1. Obtenemos el Código de Reclamo Autogenerado por Laravel
            // =================================================================
            $.ajax({
                type:'GET',
                url: $('#ajxCrearCodigoReclamo').val(),
                success: function(data) {
                    var objResponse = $.parseJSON(data);
                    jsn_num_carta_reclamo=objResponse["codigoReclamo"];

                    // =================================================================
                    // 2. Enviamos los datos al WS de SOLE
                    // =================================================================
                    var data_respuesta = null;
                    var cabeceraReclamo = {
                        'fecha_reclamo': jsn_fecha_reclamo
                        , 'num_carta_reclamo': jsn_num_carta_reclamo
                        , 'tipo_bien': jsn_tipo_bien
                        , 'tienda': jsn_tienda
                        , 'nombre_cliente': jsn_nombre_cliente
                        , 'direccion_cliente': jsn_direccion_cliente
                        , 'tipo_doc': jsn_tipo_doc
                        , 'dni_cliente': jsn_dni_cliente
                        , 'telefono_cliente': jsn_telefono_cliente
                        , 'mail_cliente': jsn_mail_cliente
                        , 'apoderado': jsn_apoderado
                        , 'codigo_producto': jsn_codigo_producto
                        , 'tipo_comprobante': jsn_tipo_comprobante
                        , 'serie_comprobante': jsn_serie_comprobante
                        , 'numero_comprobante': jsn_numero_comprobante
                        , 'fecha_factura': jsn_fecha_factura
                        , 'detalle': jsn_detalle
                        , 'codigo_departamento': jsn_cod_dpto
                        , 'codigo_provincia': jsn_cod_prov
                        , 'codigo_distrito': jsn_cod_dist
                        , 'metodo_notificacion': jsn_met_notif
                    };

                    var respuesta_bapi = null;

                    var objRecBE = JSON.stringify(cabeceraReclamo);

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "http://ts.metusa.com/ServiciosWebPVEcommerce/WS_BAPI_ECOMMERCE_SOLE.asmx/mReclamo_Insertar_Libro_Virtual_Actual",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: "{'objReclamoBE':" + objRecBE + "}",
                        success: function (response) {
                            data_respuesta = JSON.parse(response.d);
                            if (data_respuesta[0].mensaje_error == '') {
                                jsn_reclamo_procesado="1";
                                jsn_reclamo_codigo_externo=data_respuesta[0].numero_reclamo;
                            }else{
                                jsn_reclamo_procesado="0";
                                jsn_reclamo_codigo_externo="";
                            }

                            // =================================================================
                            // 3. Registramos los datos del formulario en el sistema Laravel
                            // =================================================================
                            var form_data = {
                                "_token": $("#csrf_token").val(),
                                'fecha_reclamo': jsn_fecha_reclamo,
                                'num_carta_reclamo': jsn_num_carta_reclamo,
                                'tipo_bien': jsn_tipo_bien,
                                'tienda': jsn_tienda,
                                'nombre_cliente': str_clienteNombre,
                                'appat_cliente': str_clienteAppaterno,
                                'apmat_cliente': str_clienteApmaterno,
                                'direccion_cliente': jsn_direccion_cliente,
                                'codigo_departamento': jsn_cod_dpto,
                                'codigo_provincia': jsn_cod_prov,
                                'codigo_distrito': jsn_cod_dist,
                                'tipo_doc': jsn_tipo_doc,
                                'dni_cliente': jsn_dni_cliente,
                                'telefono_cliente': jsn_telefono_cliente,
                                'mail_cliente': jsn_mail_cliente,
                                'es_mayor_edad': jsn_apoderado,
                                'codigo_producto': jsn_codigo_producto,
                                'tipo_comprobante': jsn_tipo_comprobante,
                                'serie_comprobante': jsn_serie_comprobante,
                                'numero_comprobante': jsn_numero_comprobante,
                                'fecha_factura': jsn_fecha_factura,
                                'detalle': jsn_detalle,
                                'apoderado_cliente': jsn_apoderado_cliente,
                                'metodo_notificacion': jsn_met_notif,
                                'precio_producto': jsn_precio_producto,
                                'descripcion_producto': jsn_descripcion_producto,
                                'reclamo_procesado': jsn_reclamo_procesado,
                                'reclamo_codigo_externo': jsn_reclamo_codigo_externo
                            };

                            $.ajax({
                                type:'POST',
                                url: $('#ajxRegistrarReclamo').val(),
                                data: form_data,
                                success: function(data)
                                {	var objResponse = $.parseJSON(data);
                                    var idReclamo = objResponse["idReclamo"];
                                    window.location.replace($('#enviarEmail').val());
                                },
                                error: function(jqXHR, textStatus, errorThrown)
                                {	$("#msg_error").html("<strong>ERROR:</strong> Se produjo un error en el envio: intente de nuevo").show();
                                    $("#btn_enviar").show();
                                    $("#captcha-box").show();
                                    $("#msg_enviar").hide();
                                }
                            })
                        },
                        error: function (result) {
                            //alert('ERROR: ' + result.status + ' ' + result.statusText + ' ' + result.responseText);
                            $("#msg_error").html("<strong>ERROR:</strong> Se produjo un error en el envio: intente de nuevo").show();
                            $("#btn_enviar").show();
                            $("#captcha-box").hide();
                            $("#msg_enviar").hide();
                        }
                    });
                },
                error: function(jqXHR, textStatus, errorThrown)
                {	$("#msg_error").html("<strong>ERROR:</strong> Se produjo un error en el envio: intente de nuevo").show();
                    $("#btn_enviar").show();
                    $("#captcha-box").hide();
                    $("#msg_enviar").hide();
                }

            })



        }

    }



});



var today = new Date();

var dd = today.getDate();

var mm = today.getMonth()+1;



var yyyy = today.getFullYear();

if(dd<10){

    dd='0'+dd;

}

if(mm<10){

    mm='0'+mm;

}

var today = dd+'/'+mm+'/'+yyyy;





// =====================

// LLENADO DROPDOWNLISTS

// =====================



var str_reclamoFecha_dia = "<option value='-'>Dia</option>";

var str_reclamoFecha_mes = "<option value='-'>Mes</option>";

var str_reclamoFecha_anho = "<option value='-'>Año</option>";



var valueDay = "";

for(i=1;i<=31;i++)

{	if(i<10)

{	valueDay = '0'+i;		}

else

{	valueDay = i;			}



    if(valueDay==dd)

    {	str_reclamoFecha_dia = str_reclamoFecha_dia + "<option value='"+valueDay+"' selected>"+valueDay+"</option>";	}

    else

    {	str_reclamoFecha_dia = str_reclamoFecha_dia + "<option value='"+valueDay+"'>"+valueDay+"</option>";	}



    valueoption = "";

}



var valueYear = "";

for(j=1970;j<=(new Date().getFullYear());j++)

{	if(yyyy==j)

{	str_reclamoFecha_anho = str_reclamoFecha_anho + "<option value='"+j+"' selected>"+j+"</option>";	}

else

{	str_reclamoFecha_anho = str_reclamoFecha_anho + "<option value='"+j+"'>"+j+"</option>";	}

}



if(mm=="01")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='01' selected>Enero</option>";	}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='01'>Enero</option>";	}



if(mm=="02")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='02' selected>Febrero</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='02'>Febrero</option>";		}



if(mm=="03")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='03' selected>Marzo</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='03'>Marzo</option>";		}



if(mm=="04")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='04' selected>Abril</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='04'>Abril</option>";		}



if(mm=="05")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='05' selected>Mayo</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='05'>Mayo</option>";		}



if(mm=="06")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='06' selected>Junio</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='06'>Junio</option>";		}



if(mm=="07")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='07' selected>Julio</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='07'>Julio</option>";		}



if(mm=="08")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='08' selected>Agosto</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='08'>Agosto</option>";		}



if(mm=="09")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='09' selected>Setiembre</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='09'>Setiembre</option>";		}



if(mm=="10")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='10' selected>Octubre</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='10'>Octubre</option>";		}



if(mm=="11")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='11' selected>Noviembre</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='11'>Noviembre</option>";		}



if(mm=="12")

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='12' selected>Diciembre</option>";		}

else

{	str_reclamoFecha_mes = str_reclamoFecha_mes + "<option value='12'>Diciembre</option>";		}



$("#reclamoFecha_dia").html(str_reclamoFecha_dia);

$("#reclamoFecha_mes").html(str_reclamoFecha_mes);

$("#reclamoFecha_anho").html(str_reclamoFecha_anho);





var str_docFecha_dia = "<option value='-'>Dia</option>";

var str_docFecha_mes = "<option value='-'>Mes</option>";

var str_docFecha_anho = "<option value='-'>Año</option>";



for(i=1;i<=31;i++)

{	if(i<10)

{	valueDay = '0'+i;		}

else

{	valueDay = i;			}



    str_docFecha_dia = str_docFecha_dia + "<option value='"+valueDay+"'>"+valueDay+"</option>";



    valueoption = "";

}



str_docFecha_mes = str_docFecha_mes + "<option value='01'>01</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='02'>02</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='03'>03</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='04'>04</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='05'>05</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='06'>06</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='07'>07</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='08'>08</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='09'>09</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='10'>10</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='11'>11</option>";

str_docFecha_mes = str_docFecha_mes + "<option value='12'>12</option>";



for(j=(new Date().getFullYear());j>=1980;j--)

{	str_docFecha_anho = str_docFecha_anho + "<option value='"+j+"'>"+j+"</option>";	}



$("#docFecha_dia").html(str_docFecha_dia);

$("#docFecha_mes").html(str_docFecha_mes);

$("#docFecha_anho").html(str_docFecha_anho);

$("#tipoComuReclamo").on("change", function(){
    if($(this).val() == "2"){
        $("#clienteEmail").removeAttr("required");
        $("#clienteDireccion").attr("required", "required");
    }else{
        $("#clienteEmail").attr("required", "required");
        $("#clienteDireccion").removeAttr("required");
    }
});
