## Sole - Libro de reclamaciones

#### Instalación en Servidor
Para la instalación en un servidor se recomienda clonar el proyecto desde git
- `git clone [nombreRepositorio]`
- `composer install`
- `php artisan migrate --seed`

#### Entorno local
En un entorno local no necesitarás Apache, ya que artisan trae su propio servidor
- `composer install`
- `php artisan migrate --seed`
- `npm install`
- `php artisan serve`
