/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

import $ from 'jquery';
window.$ = window.jQuery = $;

import 'jquery-ui/ui/widgets/datepicker.js';

$.datepicker.setDefaults($.datepicker.regional["es"]);


$('.datepicker').datepicker({
    dateFormat: "DD dd/mm/yy"
});

window.moment = require('moment/moment');

window.submitFilters = function () {
    let dateFrom = $('#dateFrom').val().split(' ')[1];
    let from = moment(dateFrom, "DD/MM/YYYY").format('YYYY-MM-DD');

    let dateTo = $('#dateTo').val().split(' ')[1];
    let to = moment(dateTo, "DD/MM/YYYY").format('YYYY-MM-DD');

    localStorage.setItem('dateFrom', from);
    localStorage.setItem('dateTo', to);
    window.location.href = '?from='+from+'&to='+to
};
