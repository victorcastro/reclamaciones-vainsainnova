@extends('layouts.app')

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if ($success)
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ $success }}
</div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Modificar Configuración</div>
                <div class="card-body">
                    {!! Form::model($setting, ['route' => ['configuraciones.update', $setting->id], 'method' => 'put']) !!}
                    @csrf
                    <div class="form-group mb-4">
                        <label for="Key">Key</label>
                        {!! Form::text('key', null, ['class' => 'form-control', 'id' => 'key', 'maxlength' => 30]) !!}
                    </div>
                    <div class="form-group mb-4">
                        <label for="value">Value</label>
                        {!! Form::textarea('value', null, ['class' => 'form-control', 'id' => 'value']) !!}
                    </div>

                    <div class="d-flex justify-content-between">
                        <a href="{{ route('configuraciones.index') }}" class="btn btn-sm "> <i class="fas fa-arrow-circle-left"></i> Volver a lista de configuraciones</a>
                        <button type="submit" class="btn btn-primary"> <i class="fas fa-save"></i> Guardar</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
