@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-between">
        <div class="col-md-12">
            <!-- <a href="{{ route('configuraciones.create') }}" class="btn btn-danger text-white"> <i class="fas fa-user-plus"></i> Crear nuevo </a> -->
            <br><br>
            <!--Table-->
            <table id="tablePreview" class="table">
                <!--Table head-->
                <thead>
                <tr>
                    <th>#</th>
                    <th>Key</th>
                    <th>Value</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <!--Table head-->
                <!--Table body-->
                <tbody>
                @if (count($settings) > 0)
                @foreach ($settings as $setting)
                <tr>
                    <th scope="row">{{ $setting->id }}</th>
                    <td>{{ $setting->key }}</td>
                    <td>{{ $setting->value }}</td>
                    <td class="row justify-content-between">
                        <a href="{{ route('configuraciones.edit', $setting->id) }}" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i> Editar</a>
                        <form action="{{route('configuraciones.destroy', $setting->id)}}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
                @endif
                </tbody>
                <!--Table body-->
            </table>
            <!--Table-->
        </div>
    </div>
</div>
@endsection
