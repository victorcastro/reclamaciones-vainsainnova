<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sole | Libro de Reclamaciones</title>

    <meta name="description" content="Sole | Libro de Reclamaciones" />
    <meta name="author" content="Pixellar" />

    <meta property="og:title" content="Sole | Libro de Reclamaciones" />
    <meta property="og:site_name" content="Sole | Libro de Reclamaciones" />
    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Sole | Libro de Reclamaciones" />

    <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">

    <link href="{{ asset('css/master/normalize.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/master/foundation.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/master/site-style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('js/vendor/colorbox/colorbox.css') }}" rel="stylesheet" type="text/css"  media="screen">

    <script src="{{ asset('js/vendor/modernizr.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('js/vendor/json2.js') }}"></script>

</head>

<body>

<div class="site-container">

    <header>

        @if(Session::has('loggedUser'))
        <div class="header-box">
            <div class="upper-content">
                <div class="logo-left">
                    <a href="{{ URL::action("ReclamoController@buscarReclamo") }}"><img src="{{ URL::asset('images/logoWhite.png') }}" /></a>
                </div>
                <div class="session-info">
                    <div class="line-top">
                        bienvenido
                    </div>
                    <div class="line-middle">
                        {{ mb_strtoupper(Session::get('loggedUser')->nombre) }}
                    </div>
                    <div class="line-bottom">
                        ADMINISTRADOR
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
            <div class="lower-content">
                <div class="site-grid-content">
                    <div class="row">
                        <div class="site-menu">
                            <div class="option-left">
                                <a href="{{ URL::action("ReclamoController@buscarReclamo") }}">inicio</a>
                            </div>
                            <div class="divider-left">
                                |
                            </div>
                            <div class="option-left">
                                <a href="{{ URL::action("UsuarioController@editarCuenta") }}">cuenta</a>
                            </div>
                            <div class="option-right">
                                <a href="{{ URL::action("UsuarioController@logout") }}">cerrar sesión</a>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        @endif

    </header>

    <div class="site-content">
        @yield('content')
    </div>

    <footer>

        @if(!Session::has('loggedUser'))
        <!--
        <div class="footer-box-index">
            <div class="texto">
                © Pontificia Universidad Católica del Perú - 2015. Todos los derechos reservados.<br>
                Av. Universitaria 1801, San Miguel, Lima 32, Perú | Teléfono (511) 626-2000
            </div>
        </div>

        <div class="footer-box-index-mobile">
            <div class="texto">
                © Pontificia Universidad Católica del Perú - 2015.<br>
                Todos los derechos reservados.<br>
                Av. Universitaria 1801, San Miguel, Lima 32, Perú <br>Teléfono (511) 626-2000
            </div>
        </div>
        -->
        @else
        <div class="footer-box">
            <div class="logo-left">
                <img src="{{ URL::asset('images/logoFooterSOLE.png') }}" />
            </div>
            <div class="options-content">
                <div class="option-right">
                    <a href="{{ URL::action("UsuarioController@logout") }}">cerrar sesión</a>
                </div>
                <div class="option-right">
                    <a href="{{ URL::action("UsuarioController@editarCuenta") }}">cuenta</a>
                </div>
                <div class="option-right">
                    <a href="{{ URL::action("ReclamoController@buscarReclamo") }}">inicio</a>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>

        <div class="footer-box-mobile">
            <div class="row fullWidth">
                <div class="columns small-12">
                    <div class="logo-content">
                        <img src="{{ URL::asset('images/logoFooterSOLE.png') }}" />
                    </div>
                </div>
                <div class="columns small-12">
                    <div class="options-content">
                        <div class="option">
                            <a href="{{ URL::action("UsuarioController@logout") }}">cerrar sesión</a>
                        </div>
                        <div class="option">
                            <a href="{{ URL::action("UsuarioController@editarCuenta") }}">cuenta</a>
                        </div>
                        <div class="option">
                            <a href="{{ URL::action("ReclamoController@buscarReclamo") }}">inicio</a>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            </div>
        </div>

        @endif


    </footer>

</div>

<script src="{{ asset('js/site-script.js') }}"></script>
<script src="{{ asset('js/vendor/colorbox/jquery.colorbox-min.js') }}"></script>
<script src="{{ asset('js/foundation.min.js') }}"></script>
<script>
    $('.site-grid-content').foundation();
</script>

</body>
</html>
