@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-3 mb-4">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Filtros:</span>
            </h4>
            <form id="reclamos-filters" type="GET" onsubmit="event.preventDefault(); submitFilters();">
                <ul class="list-group mb-3">
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0 font-weight-bold">Desde:</h6>
                            {!! Form::text('datefrom', $viewDateFrom, ['class' => 'form-control-plaintext datepicker text-muted', 'id' => 'dateFrom', 'maxlength' => 20]) !!}
                        </div>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0 font-weight-bold">Hasta:</h6>
                            {!! Form::text('dateto', $viewDateTo, ['class' => 'form-control-plaintext datepicker text-muted', 'id' => 'dateTo', 'maxlength' => 20]) !!}
                        </div>
                    </li>
                </ul>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-sm btn-success">Aplicar</button>
                </div>
            </form>
        </div>
        <div class="col-sm-9">
            <div class="row mb-3 ">
                @if (count($reclamos) > 0)
                    @foreach ($reclamos as $reclamo)
                    <div class="col-sm-12 col-md-4 mb-4">
                        <div class="card box-shadow" style="height: 100%">
                            <div class="card-body">
                                <h5 class="card-title font-weight-bold">
                                    <a href="{{ route('reclamos.show', $reclamo->idReclamo) }}">
                                        <i class="fas fa-list-alt"></i>&nbsp; {{ $reclamo->reclamoNumero }}
                                    </a>
                                </h5>
                                <h3></h3>
                                <h5>
                                    <span class="font-weight-bold">{{ $reclamo->codigoTipoBien === 1 ? 'Servicio' : 'Producto' }}:</span>
                                    <span>{{ $reclamo->productoDescripcion }}</span>
                                </h5>
                                <p>{{ $reclamo->nombre_completo_cliente }}</p>
                            </div>
                            <div class="card-footer">
                                <small class="text-muted">{{ $reclamo->datefrom }}</small>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-12">

                        {!! $reclamos->appends(['from' => $inputFrom, 'to' => $inputTo])->links() !!}
                    </div>
                @else
                <div class="">
                    <h5 class="text-center m-4">No Hay registros para la fecha seleccionada</h5>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
