@extends('layouts.master')

@section('content')
<link href="{{ asset('css/master/foundation.css') }}" rel="stylesheet">


<script src='https://www.google.com/recaptcha/api.js'></script>



<div class="site">



    <div class="header-form-box">

        <div class="logo-form-left">

            <a href="http://www.sole.com.pe"><img src="{{ URL::asset('images/logoForm.png') }}" /></a>

        </div>

        <div class="logo-form-right">

            <a href="http://www.sole.com.pe">Volver a www.sole.com.pe</a>

        </div>

        <div style="clear:both;"></div>

    </div>



    <div class="site-grid-content site-content">



        <div class="row site-content-padding">

            <div class="columns small-12">

                <div class="row">

                    <div class="columns small-12">

                        <div class="form-header-block">

                            <div class="row">

                                <div id="form-logo-reclamaciones" class="columns small-6">

                                    <img src="{{ URL::asset('images/logoReclamaciones.png') }}" />

                                </div>

                                <div class="columns small-6">

                                    <div id="form-empresa-top">MT INDUSTRIAL S.A.C.</div>

                                    <div id="form-empresa-middle">RUC Nº <strong>20555190132</strong></div>

                                    <div id="form-empresa-bottom">Av Argentina 2100 - Lima, Per&uacute;</div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="columns small-12">

                <div class="row">

                    <div class="columns small-12">

                        <div class="form-title">DATOS DEL CLIENTE</div>

                    </div>

                </div>

                <div class="row">

                    <div class="columns small-12 medium-4">

                        <div id="reclamoFecha_title" class="form-label">Fecha</div>

                        <div id="reclamoFecha_control" class="form-control">

                            <div class="row">

                                <div class="columns small-12 medium-3">

                                    <select id="reclamoFecha_dia" name="nac_dia" class="form-select" disabled>

                                        <option selected="selected" value="-">Dia</option>

                                    </select>

                                </div>

                                <div class="columns small-12 medium-5">

                                    <select id="reclamoFecha_mes" name="nac_mes" class="form-select" disabled>

                                        <option selected="selected" value="-">Mes</option>

                                    </select>

                                </div>

                                <div class="columns small-12 medium-4">

                                    <select id="reclamoFecha_anho" name="nac_anho" class="form-select" disabled>

                                        <option selected="selected" value="-">A&ntilde;o</option>

                                    </select>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="columns small-12 medium-8">

                        <div class="form-label">Tienda</div>

                        <div class="form-control">

                            <select id="tienda" name="tienda" class="form-select">

                                @foreach($lstTiendas as $tienda)

                                <option value="{{ $tienda->codigoTienda }}">{!! $tienda->nombre !!}</option>

                                @endforeach

                            </select>

                        </div>

                    </div>

                    <!--<div class="columns small-12 medium-4">

                        <div id="reclamoNumero_title" class="form-label">Nro de reclamo</div>

                        <div id="reclamoNumero_control" class="form-control">

                            <input type="text" id="reclamoNumero" name="reclamoNumero" class="form-textbox" />

                        </div>

                    </div>-->

                </div>

                <div class="row">

                    <div class="columns small-12">

                        <div id="tipoComuReclamo_title" class="form-label">¿Como desea que nos comuniquemos con Ud. acerca de su reclamo?:</div>

                        <div id="tipoComuReclamo_control" class="form-control">

                            <select id="tipoComuReclamo" name="tipoComuReclamo" class="form-select">

                                <option value="1">Mediante Correo Electronico</option>

                                <option value="2">Mediante Dirección Proporcionada</option>

                            </select>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="columns small-12 medium-4">

                        <div id="clienteNombre_title" class="form-label">Nombre</div>

                        <div id="clienteNombre_control" class="form-control">

                            <input type="text" id="clienteNombre" name="clienteNombre" class="form-textbox" />

                        </div>

                    </div>

                    <div class="columns small-12 medium-4">

                        <div id="clienteAppaterno_title" class="form-label">Apellido paterno</div>

                        <div id="clienteAppaterno_control" class="form-control">

                            <input type="text" id="clienteAppaterno" name="clienteAppaterno" class="form-textbox" />

                        </div>

                    </div>

                    <div class="columns small-12 medium-4">

                        <div id="clienteApmaterno_title" class="form-label">Apellido materno</div>

                        <div id="clienteApmaterno_control" class="form-control">

                            <input type="text" id="clienteApmaterno" name="clienteApmaterno" class="form-textbox" />

                        </div>

                    </div>



                </div>

                <div class="row">

                    <div class="columns small-12 medium-4">

                        <div class="form-label">Doc. Identidad</div>

                        <div class="form-control">

                            <select id="tipoDocumento" name="tipoDocumento" class="form-select">

                                @foreach($lstTipoDocumento as $tipoDocumento)

                                <option value="{{ $tipoDocumento->codigoTipoDocumento }}">{!! $tipoDocumento->nombre !!}</option>

                                @endforeach

                            </select>

                        </div>

                    </div>

                    <div class="columns small-12 medium-4">

                        <div id="clienteNumDoc_title" class="form-label">N&uacute;mero</div>

                        <div id="clienteNumDoc_control" class="form-control">

                            <input type="text" id="clienteNumDoc" name="clienteNumDoc" class="form-textbox" onkeypress="return isNumberKey(event)" />

                        </div>

                    </div>

                    <div class="columns small-12 medium-4">

                        <div id="clienteTelefono_title" class="form-label">Tel&eacute;fono</div>

                        <div id="clienteTelefono_control" class="form-control">

                            <input type="text" id="clienteTelefono" name="clienteTelefono" class="form-textbox" onkeypress="return isNumberKey(event)" />

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div id="correo_div" class="columns small-12 medium-12">

                        <div id="clienteEmail_title" class="form-label">Correo Electr&oacute;nico</div>

                        <div id="clienteEmail_control" class="form-control">

                            <input type="text" id="clienteEmail" name="clienteEmail" class="form-textbox" required />

                        </div>

                    </div>

                    <div id="direccion_div" class="columns small-12 medium-12">

                        <div id="clienteDireccion_title" class="form-label">Direcci&oacute;n</div>

                        <div id="clienteDireccion_control" class="form-control">

                            <input type="text" id="clienteDireccion" name="clienteDireccion" class="form-textbox" />

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="columns small-12 medium-4">

                        <div id="dptoDir_title" class="form-label">Departamento</div>

                        <div id="dptoDir_control" class="form-control">

                            <select id="dptoDir" name="dptoDir" class="form-select">

                            </select>

                        </div>

                    </div>

                    <div class="columns small-12 medium-4">

                        <div id="provDir_title" class="form-label">Provincia</div>

                        <div id="provDir_control" class="form-control">

                            <select id="provDir" name="provDir" class="form-select">

                            </select>

                        </div>

                    </div>

                    <div class="columns small-12 medium-4">

                        <div id="distDir_title" class="form-label">Distrito</div>

                        <div id="distDir_control" class="form-control">

                            <select id="distDir" name="distDir" class="form-select">

                            </select>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="columns small-12 medium-4">

                        <div class="form-label">Mayor de edad</div>

                        <div class="form-control">

                            <select id="clienteEsMayorEdad" name="clienteEsMayorEdad" class="form-select">

                                <option value="S">Si</option>

                                <option value="N">No</option>

                            </select>

                        </div>

                    </div>

                    <div class="columns small-12 medium-8">

                        <div id="clienteApoderado_title" class="form-label">Nombre apoderado</div>

                        <div id="clienteApoderado_control" class="form-control">

                            <input type="text" id="clienteApoderado" name="clienteApoderado" class="form-textbox" />

                        </div>

                    </div>

                </div>

            </div>





            <div class="columns small-12">

                <div class="row">

                    <div class="columns small-12">

                        <div class="form-title">IDENTIFICAR PRODUCTO / SERVICIO</div>

                    </div>

                </div>

                <div class="row">

                    <div class="columns small-12">

                        <div class="form-label">Este reclamo est&aacute; relacionado a un:</div>

                        <div class="form-control">

                            <select id="tipoBien" name="tipoBien" class="form-select">

                                @foreach($lstTipoBien as $tipoBien)

                                <option value="{{ $tipoBien->codigoTipoBien }}">{!! $tipoBien->nombre !!}</option>

                                @endforeach

                            </select>

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="columns small-12">

                        <div class="form-subtitle">DOCUMENTO DE COMPRA</div>

                    </div>

                    <div class="columns small-12 medium-3">

                        <div id="docFecha_title" class="form-label">Fecha (dd/mm/aaaa)</div>

                        <div id="docFecha_control" class="form-control">



                            <div class="row">

                                <div class="columns small-12 medium-4">

                                    <select id="docFecha_dia" name="nac_dia" class="form-select">

                                        <option selected="selected" value="-">dd</option>

                                    </select>

                                </div>

                                <div class="columns small-12 medium-4">

                                    <select id="docFecha_mes" name="nac_mes" class="form-select">

                                        <option selected="selected" value="-">mm</option>

                                    </select>

                                </div>

                                <div class="columns small-12 medium-4">

                                    <select id="docFecha_anho" name="nac_anho" class="form-select">

                                        <option selected="selected" value="-">aaaa</option>

                                    </select>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="columns small-12 medium-3">

                        <div class="form-label">Tipo</div>

                        <div class="form-control">

                            <select id="tipoComprobante" name="tipoComprobante" class="form-select">

                                @foreach($lstTipoComprobante as $tipoComprobante)

                                <option value="{{ $tipoComprobante->codigoTipoComprobante }}">{!! $tipoComprobante->nombre !!}</option>

                                @endforeach

                            </select>

                        </div>

                    </div>

                    <div class="columns small-12 medium-3">

                        <div id="comprobanteSerie_title" class="form-label">Serie</div>

                        <div id="comprobanteSerie_control" class="form-control">

                            <input type="text" id="comprobanteSerie" name="comprobanteSerie" class="form-textbox" />

                        </div>

                    </div>

                    <div class="columns small-12 medium-3">

                        <div id="comprobanteNumero_title" class="form-label">N&uacute;mero</div>

                        <div id="comprobanteNumero_control" class="form-control">

                            <input type="text" id="comprobanteNumero" name="comprobanteNumero" class="form-textbox" onkeypress="return isNumberKey(event)" />

                        </div>

                    </div>

                </div>

                <div class="row">

                    <div class="columns small-12">

                        <div class="form-subtitle">PRODUCTO</div>

                    </div>

                    <div class="columns small-12 medium-3">

                        <div id="productoCodigo_title" class="form-label">C&oacute;digo</div>

                        <div id="productoCodigo_control" class="form-control">

                            <input type="text" id="productoCodigo" name="productoCodigo" class="form-textbox" />

                        </div>

                    </div>

                    <div class="columns small-12 medium-3">

                        <div id="productoPrecio_title" class="form-label">Precio</div>

                        <div id="productoPrecio_control" class="form-control">

                            <input type="text" id="productoPrecio" name="productoPrecio" class="form-textbox" onkeypress="return isNumberKey(event)" />

                        </div>

                    </div>

                    <div class="columns small-12 medium-6">

                        <div id="productoDescripcion_title" class="form-label">Descripci&oacute;n</div>

                        <div id="productoDescripcion_control" class="form-control">

                            <input type="text" id="productoDescripcion" name="productoDescripcion" class="form-textbox" />

                        </div>

                    </div>

                </div>

            </div>





            <div class="columns small-12">

                <div class="row">

                    <div class="columns small-12">

                        <div class="form-title">DETALLE DEL RECLAMO</div>

                    </div>

                </div>

                <div class="row">

                    <div class="columns small-12">

                        <div id="reclamoDetalle_title" class="form-label">Detalle</div>

                        <div id="reclamoDetalle_control" class="form-control">

                            <textarea id="reclamoDetalle" rows="15" maxlength="2500" name="reclamoDetalle" class="form-textarea"></textarea>

                        </div>

                        <div class="form-counter">

                            <div id="textarea_feedback"></div>

                        </div>

                    </div>



                    <script>

                        var max_ini = 2500;

                        var len_ini = $('#reclamoDetalle').val().length;

                        var char_ini = max_ini - len_ini;

                        $('#textarea_feedback').text(char_ini + '/'+max_ini+' caracteres disponibles');



                        $('#reclamoDetalle').keyup(function (e) {

                            var max = 2500;

                            var len = ($(this).val().length);

                            var char = max - len;



                            if (len >= max) {

                                $('#textarea_feedback').text(char + '/'+max+' caracteres disponibles');

                                $('#textarea_feedback').css({"color":"red"});

                            } else {

                                $('#textarea_feedback').text(char + '/'+max+' caracteres disponibles');

                                $('#textarea_feedback').css({"color":"#0a3697"});

                            }

                        });



                    </script>



                </div>

            </div>



            <div class="columns small-12">

                <div class="row">

                    <div class="columns small-12 medium-6">

                        <div>

                            <div id="btn_enviar" class="button-div">enviar</div>

                        </div>

                        <div>

                            <div id="msg_error_captcha"></div>

                            <div id="msg_error"></div>

                            <div id="msg_enviar" style="display:none;">

                                <div class="image-right">

                                    <img src="{{ URL::asset('images/loader.gif') }}" />

                                </div>

                                <div class="text-right"><strong>ENVIANDO INFORMACI&Oacute;N</strong></div>

                                <div style="clear:both;"></div>

                            </div>



                        </div>

                    </div>

                    <div class="columns small-12 medium-6">

                        <div style="float:right;" class="g-recaptcha" data-sitekey="6LdYhREUAAAAAOXBfddtv0MhVVDq6igWzK2uTGdn"></div>

                        <div style="clear:both;"></div>

                    </div>

                </div>

            </div>

        </div>



    </div>



    <div class="footer-form-box">

        <div class="site-grid-content">

            <div class="row">

                <div class="logo-form-left">

                    <img src="{{ URL::asset('images/footerLeft.png') }}" />

                </div>

                <div class="logo-form-right">

                    <img src="{{ URL::asset('images/footerRight.png') }}" />

                </div>

                <div style="clear:both;"></div>

            </div>
        </div>
    </div>
</div>

<input id="csrf_token" type="text" value="{{ csrf_token() }}">
<input id="ajxRegistrarReclamo" type="text" value="{{ URL::action('ReclamosController@ajxRegistrarReclamo') }}">
<input id="enviarEmail" type="text" value="{{ URL::action('MailController@enviarEmail') }}">
<input id="ajxCrearCodigoReclamo" type="text" value="{{ URL::action('ReclamosController@ajxCrearCodigoReclamo') }}">

<script src="{{ asset('js/formcreate.js') }}" ></script>







@endsection

