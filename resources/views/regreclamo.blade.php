@extends('layouts.master')

@section('content')


<div class="site">

    <div class="header-form-box">
        <div class="logo-form-left">
            <a href="http://www.sole.com.pe"><img src="{{ URL::asset('images/logoForm.png') }}" /></a>
        </div>
        <div class="logo-form-right">
            <a href="http://www.sole.com.pe">Volver a www.sole.com.pe</a>
        </div>
        <div style="clear:both;"></div>
    </div>
    
    <div class="site-grid-content site-content">
        <div class="row site-content-padding">
            <div class="columns small-12">
                <br><br>
                <div class="registro-line-div">
                	<div class="registro-image-div">
                    	<img src="{{ URL::asset('images/write.png') }}" />
                    </div>
                </div>
                <div class="registro-line-div">
                	Muchas gracias, <strong>{!! $resultados["nombrecliente"] !!}</strong>
                </div>
                <div class="registro-line-div">
                	Su reclamo Nro. <strong>{!! $resultados["numreclamo"] !!}</strong> ha sido registrado exitosamente, el mismo que será revisado para ser atendido a la brevedad.
                </div>
                <div class="registro-line-div">
                	<a target="_blank" href="/reclamos/{!! $resultados['idReclamo'] !!}">Descargar Reclamo</a>
            	</div>
            </div>    
        </div>
    </div>
    
    <style>
		.registro-line-div{
			font-family: 'PTSansNarrow-Regular';
			font-size:1.5em;
			padding-top:5px;
			width:100%;
			text-align:center;
		}
		
		.registro-image-div{
			width:100%;
			text-align:center;
		}
		
		.registro-image-div img{
			width:100%;
			max-width:150px;
			min-width:50px;
		}
	</style>
    
    <div class="footer-form-box">
        <div class="site-grid-content">
            <div class="row">
                <div class="logo-form-left">
                    <img src="{{ URL::asset('images/footerLeft.png') }}" />
                </div>
                <div class="logo-form-right">
                    <img src="{{ URL::asset('images/footerRight.png') }}" />
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>

</div>

@endsection