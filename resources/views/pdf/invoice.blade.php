<style>
    .form-title{
        font-family: 'PTSansNarrow-Regular';
        font-size:1.3em;
        font-weight:bold;
        padding-top:10px;
        padding-bottom:10px;

    }

    .form-subtitle{
        font-family: 'PTSansNarrow-Regular';
        font-size:1.1em;
        padding-top:5px;
        padding-bottom:5px;
    }

    .form-label{
        font-family: 'PTSansNarrow-Regular';
    }

    .form-control{
        font-family: 'PTSansNarrow-Regular';
        border:1px #CACACA solid;
        padding:5px 10px;
        margin-bottom:10px;
    }

    #form-logo-reclamaciones img{
        /*min-width:80px;*/
        max-width:350px;
        width:100%;
    }

    #form-empresa-top{
        font-family: 'PTSansNarrow-Regular';
        width:100%;
        text-align:right;
        font-size:1em;
        line-height:1em;
        padding-top:4px;
    }

    #form-empresa-middle{
        font-family: 'PTSansNarrow-Regular';
        width:100%;
        text-align:right;
        font-size:0.8em;
        line-height:0.8em;
        padding:3px 0 5px 0;
    }

    #form-empresa-bottom{
        font-family: 'PTSansNarrow-Regular';
        width:100%;
        text-align:right;
        font-size:0.7em;
        line-height:0.7em;
    }

    .form-header-block{
        border-bottom:1px #CACACA solid;
        padding-bottom:20px;
        margin-bottom:20px;
    }

</style>

<div class="site-grid-content">

    <div class="row site-content-padding">
        <div class="columns small-12">
            <div class="row">
                <div class="columns small-12">
                    <div class="form-header-block">
                        <div class="row">
                            <div id="form-logo-reclamaciones" class="columns small-6">
                                <img src="{{ URL::asset('images/logoReclamaciones.png') }}" />
                            </div>
                            <div class="columns small-6">
                                <div id="form-empresa-top">MT INDUSTRIAL S.A.C.</div>
                                <div id="form-empresa-middle">RUC N潞 <strong>20555190132</strong></div>
                                <div id="form-empresa-bottom">Av Argentina 2100 - Lima, Per&uacute;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="columns small-12">
            <div class="row">
                <div class="columns small-12">
                    <div class="form-title">DATOS DEL CLIENTE</div>
                </div>
            </div>
            <div class="row">
                <div class="columns medium-4">
                    <div class="form-label">Fecha</div>
                    <div class="form-control">{!! Carbon\Carbon::parse($data["reclamo"]->reclamoFecha)->format('d/m/Y') !!}</div>
                </div>
                <div class="columns medium-4">
                    <div class="form-label">Tienda</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->tienda->nombre) !!}</div>
                </div>
                <div class="columns medium-4">
                    <div class="form-label">Nro de reclamo</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->reclamoNumero) !!}</div>
                </div>
            </div>
            <div class="row">
                <div class="columns medium-4">
                    <div class="form-label">Tipo de Comunicacion</div>
                    <div class="form-control">
                        @if($data["reclamo"]->clienteMetodoNotificacion == "1")
                        Mediante Correo Electronico
                        @else
                        Mediante Direcci贸n Proporcionada
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="columns small-12">
                    <div class="form-label">Nombres y apellidos</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->clienteNombre) !!} {!! htmlentities($data["reclamo"]->clienteAppaterno) !!} {!! htmlentities($data["reclamo"]->clienteApmaterno) !!}</div>
                </div>
            </div>
            <div class="row">
                <div class="columns medium-4">
                    <div class="form-label">Doc. Identidad</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->tipoDocumento->nombre) !!}</div>
                </div>
                <div class="columns medium-4">
                    <div class="form-label">N&uacute;mero</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->clienteNumDoc) !!}</div>
                </div>
                @if($data["reclamo"]->clienteEmail != "")
                <div class="columns medium-4">
                    <div class="form-label">Correo Electr&oacute;nico</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->clienteEmail) !!}</div>
                </div>
                @endif
            </div>
            <div class="row">
                <div class="columns medium-8">
                    <div class="form-label">Departamento</div>
                    <div id="inputDpto" class="form-control">{!! htmlentities($data["ubigeo"]["departamento"]) !!}</div>
                </div>
                <div class="columns medium-4">
                    <div class="form-label">Provincia</div>
                    <div id="inputProv" class="form-control">{!! htmlentities($data["ubigeo"]["provincia"]) !!}</div>
                </div>
                <div class="columns medium-4">
                    <div class="form-label">Distrito</div>
                    <div id="inputDist" class="form-control">{!! htmlentities($data["ubigeo"]["distrito"]) !!}</div>
                </div>
            </div>
            <div class="row">
                @if($data["reclamo"]->clienteDireccion != "")
                <div class="columns medium-8">
                    <div class="form-label">Direcci&oacute;n</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->clienteDireccion) !!}</div>
                </div>
                @endif
                <div class="columns medium-4">
                    <div class="form-label">Tel&eacute;fono</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->clienteTelefono) !!}</div>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="columns medium-4">
                    <div class="form-label">Mayor de edad</div>
                    <div class="form-control">
                        @if ($data["reclamo"]->clienteEsMayorEdad == "S")
                        Si
                        @else
                        No
                        @endif
                    </div>
                </div>
                <div class="columns medium-8">
                    <div class="form-label">Nombre apoderado</div>
                    <div class="form-control">
                        @if ($data["reclamo"]->clienteApoderado == "")
                        &nbsp;
                        @else
                        {!! htmlentities($data["reclamo"]->clienteApoderado) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>


        <div class="columns small-12">
            <div class="row">
                <div class="columns small-12">
                    <div class="form-title">IDENTIFICAR PRODUCTO / SERVICIO</div>
                </div>
            </div>
            <div class="row">
                <div class="columns small-12">
                    <div class="form-label">Este reclamo est&aacute; relacionado a un:</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->tipoBien->nombre) !!}</div>
                </div>
            </div>
            <div class="row">
                <div class="columns small-12">
                    <div class="form-subtitle">DOCUMENTO DE COMPRA</div>
                </div>
                <div class="columns medium-3">
                    <div class="form-label">Fecha (dd/mm/aaaa)</div>
                    <div class="form-control">{!! Carbon\Carbon::parse($data["reclamo"]->comprobanteFecha)->format('d/m/Y') !!}</div>
                </div>
                <div class="columns medium-3">
                    <div class="form-label">Tipo</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->tipoComprobante->nombre) !!}</div>
                </div>
                <div class="columns medium-3">
                    <div class="form-label">Serie</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->comprobanteSerie) !!}</div>
                </div>
                <div class="columns medium-3">
                    <div class="form-label">N&uacute;mero</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->comprobanteNumero) !!}</div>
                </div>
            </div>
            <div class="row">
                <div class="columns small-12">
                    <div class="form-subtitle">PRODUCTO</div>
                </div>
                <div class="columns medium-3">
                    <div class="form-label">C&oacute;digo</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->productoCodigo) !!}</div>
                </div>
                <div class="columns medium-3">
                    <div class="form-label">Precio</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->productoPrecio) !!}</div>
                </div>
                <div class="columns medium-6">
                    <div class="form-label">Descripci&oacute;n</div>
                    <div class="form-control">{!! htmlentities($data["reclamo"]->productoDescripcion) !!}</div>
                </div>
            </div>
        </div>


        <div class="columns small-12">
            <div class="row">
                <div class="columns small-12">
                    <div class="form-title">DETALLE DEL RECLAMO</div>
                </div>
            </div>
            <div class="row">
                <div class="columns small-12">
                    <div class="form-label">Detalle</div>
                    <div class="form-control" style="word-wrap: break-word;">
                        @if ($data["reclamo"]->reclamoDetalle == "")
                        <font color="#FFFFFF">-</font>
                        @else
                        {!! htmlentities($data["reclamo"]->reclamoDetalle) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
