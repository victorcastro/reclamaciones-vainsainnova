Buenos días:<br>

El reclamo <strong>{{ $numreclamo }}</strong> del cliente <strong>{{ $nombrecliente }}</strong> ha sido registrado exitosamente, el mismo que será revisado para ser atendido a la brevedad.<br><br>

<strong>Pdta:</strong> Este buzón es de envío automático por favor no responder.
