@extends('layouts.empty')

@section('content')
<main class="p-4">
    <div class="container pt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-5 mb-4">
                        <img src="{{ asset('img/commons/logo_reclamaciones.png') }}" width="100%">
                    </div>
                    <div class="col-sm-7">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group ">
                                <label for="email" class="font-weight-bold">{{ __('Usuario') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group ">
                                <label for="password" class="font-weight-bold">{{ __('Contraseña') }}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="flex-column">
                                    <div class="">
                                    @if (Route::has('password.request'))
                                    <a class="" href="{{ route('password.request') }}">
                                        {{ __('¿Olvidaste la contraseña?') }}
                                    </a>
                                    @endif
                                    </div>
                                    <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-lg btn-primary">
                                        {{ __('Ingresar') }} &nbsp;<i class="fas fa-sign-in-alt"></i>
                                    </button>
                                    </div>
                                </div>


                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
