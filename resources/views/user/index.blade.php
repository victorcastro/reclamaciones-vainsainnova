@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-between">
        <div class="col-md-12">
            <a href="{{ route('user.create') }}" class="btn btn-danger text-white"> <i class="fas fa-user-plus"></i> Crear nuevo usuario </a>
            <br><br>
            <!--Table-->
            <table id="tablePreview" class="table">
                <!--Table head-->
                <thead>
                <tr>
                    <th>#</th>
                    <th>Rol</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Correo</th>
                    <th>Celular</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <!--Table head-->
                <!--Table body-->
                <tbody>
                @if (count($users) > 0)
                    @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <td>
                            @foreach ($user->roles as $role)
                            <span class="badge badge-primary badge-pill">{{ $role->name }}</span>
                            @endforeach
                        </td>
                        <td> {{ $user->name }} </td>
                        <td>{{ $user->first_lastname }} {{ $user->second_lastname }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->mobile }}</td>
                        <td class="row justify-content-between">
                            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-sm btn-warning"><i class="fas fa-edit"></i> Editar</a>
                            <form action="{{route('user.destroy', $user->id)}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                 @endif
                </tbody>
                <!--Table body-->
            </table>
            <!--Table-->
        </div>
    </div>
</div>
@endsection
