@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Cuenta de usuario</div>
                <div class="card-body">
                    {!! Form::open(['route' => ['user.store'], 'method' => 'post']) !!}
                        @csrf
                        <div class="form-group mb-4">
                            <label for="name">Nombres</label>
                            {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'maxlength' => 25]) !!}
                        </div>
                        <div class="form-group mb-4">
                            <label for="first_lastname">Apellido Paterno</label>
                            {!! Form::text('first_lastname', null, ['class' => 'form-control', 'id' => 'first_lastname', 'maxlength' => 20]) !!}
                        </div>
                        <div class="form-group mb-4">
                            <label for="second_lastname">Apellido Materno</label>
                            {!! Form::text('second_lastname', null, ['class' => 'form-control', 'id' => 'second_lastname', 'maxlength' => 20]) !!}
                        </div>
                        <div class="form-group mb-4">
                            <label for="mobile">Celular</label>
                            {!! Form::text('mobile', null, ['class' => 'form-control', 'id' => 'mobile', 'maxlength' => 30]) !!}
                        </div>
                        <div class="form-group mb-4">
                            <label for="email">Correo</label>
                            {!! Form::text('email', null, ['class' => 'form-control', 'id' => 'email', 'maxlength' => 30]) !!}
                        </div>
                        <div class="form-group mb-4">
                            <label for="password">Contraseña</label>
                            {!! Form::password('password', ['class' => 'form-control', 'maxlength' => 20]) !!}
                        </div>

                    <div class="form-group mb-4">
                        <label for="password">Perfil</label>
                        {!! Form::select('role', $roles, '', ['class' => 'form-control']) !!}
                    </div>

                        <div class="d-flex justify-content-between">
                            <a href="{{ route('user.index') }}" class="btn btn-sm "> <i class="fas fa-arrow-circle-left"></i> Volver a lista de usuario</a>
                            <button type="submit" class="btn btn-primary"> <i class="fas fa-save"></i> Guardar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
