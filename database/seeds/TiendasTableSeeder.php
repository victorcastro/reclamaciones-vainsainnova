<?php

use Illuminate\Database\Seeder;

class TiendasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tienda')->insert([
            [
                'codigoTienda' => '0',
                'nombre' => 'Tienda Sole E-Commerce',
                'order_field' => 1
            ],
            [
                'codigoTienda' => '1',
                'nombre' => 'Tienda Sole Miraflores',
                'order_field' => 5
            ],
            [
                'codigoTienda' => '10',
                'nombre' => 'CC Real Plaza - Salaverry',
                'order_field' => 9
            ],
            [
                'codigoTienda' => '11',
                'nombre' => 'CC Real Plaza - Primavera',
                'order_field' => 8
            ],
            [
                'codigoTienda' => '13',
                'nombre' => 'Tienda Sole Callao (Local Principal)',
                'order_field' => 2
            ],
            [
                'codigoTienda' => '14',
                'nombre' => 'Tienda Sole San Miguel',
                'order_field' => 4
            ],
            [
                'codigoTienda' => '15',
                'nombre' => 'CC Plaza Norte - Independencia',
                'order_field' => 10
            ],
            [
                'codigoTienda' => '16',
                'nombre' => 'CC Mall del Sur - San Juan de Miraflores',
                'order_field' => 11
            ],
            [
                'codigoTienda' => '17',
                'nombre' => 'CC Mall Aventura Porongoche - Arequipa',
                'order_field' => 15
            ],
            [
                'codigoTienda' => '18',
                'nombre' => 'CC Jockey Plaza',
                'order_field' => 16
            ],
            [
                'codigoTienda' => '19',
                'nombre' => 'CC Real Plaza - Puruchuco Ate',
                'order_field' => 17
            ],
            [
                'codigoTienda' => '3',
                'nombre' => 'CC Mall Aventura - Santa Anita',
                'order_field' => 6
            ],
            [
                'codigoTienda' => '5',
                'nombre' => 'Tienda Sole Asia',
                'order_field' => 3
            ],
            [
                'codigoTienda' => '6',
                'nombre' => 'CC Mega Plaza - Independencia',
                'order_field' => 7
            ],
            [
                'codigoTienda' => '7',
                'nombre' => 'CC Mall Plaza - Trujillo',
                'order_field' => 12
            ],
            [
                'codigoTienda' => '8',
                'nombre' => 'CC Real Plaza - Piura',
                'order_field' => 13
            ],
            [
                'codigoTienda' => '9',
                'nombre' => 'CC Real Plaza - Cusco',
                'order_field' => 14
            ]
        ]);
    }
}
