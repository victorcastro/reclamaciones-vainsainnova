<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_comprobante')->insert([
           [
               'codigoTipoComprobante' => 'BOL',
               'nombre' => 'Boleta'
           ],
            [
                'codigoTipoComprobante' => 'FAC',
                'nombre' => 'Factura'
            ]
        ]);

        DB::table('tipo_documento')->insert([
            [
                'codigoTipoDocumento' => '01',
                'nombre' => 'DNI'
            ],
            [
                'codigoTipoDocumento' => '02',
                'nombre' => 'Carnet de Extranjería'
            ],
            [
                'codigoTipoDocumento' => '06',
                'nombre' => 'RUC'
            ],
            [
                'codigoTipoDocumento' => '07',
                'nombre' => 'Pasaporte'
            ]
        ]);

        DB::table('tipo_bien')->insert([
            [
                'codigoTipoBien' => '1',
                'nombre' => 'Servicio'
            ],
            [
                'codigoTipoBien' => '2',
                'nombre' => 'Producto'
            ]
        ]);
    }
}
