<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $victorCastro = User::create([
            'name' => 'Victor',
            'first_lastname' => 'Castro',
            'second_lastname' => 'Contreras',
            'email' => 'victor@castrocontreras.com',
            'mobile' => '+51922661412',
            'password' => bcrypt('castro002')
        ]);

        $permsUserRead = Permission::create(['name'=> 'User Read']);
        $permsUserWrite = Permission::create(['name'=> 'User Write']);
        $permsUserDelete = Permission::create(['name'=> 'User Delete']);

        Role::create(['name' => 'SuperAdmin']);
        Role::create(['name' => 'Admin']);
        $rolUser = Role::create(['name' => 'User']);

        $rolUser->givePermissionTo($permsUserRead);
        $rolUser->givePermissionTo($permsUserWrite);
        $rolUser->givePermissionTo($permsUserDelete);

        $victorCastro->assignRole('SuperAdmin');

    }
}
