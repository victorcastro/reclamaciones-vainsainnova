<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTiendaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tienda', function(Blueprint $table)
		{
			$table->string('codigoTienda', 10)->primary();
			$table->string('nombre', 250)->nullable();
			$table->integer('order_field')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tienda');
	}

}
