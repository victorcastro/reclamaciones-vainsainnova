<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReclamoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reclamo', function(Blueprint $table)
		{
			$table->integer('idReclamo', true);
			$table->string('codigoTienda', 10)->index();
			$table->char('codigoTipoBien', 1)->index();
			$table->char('codigoTipoDocumento', 2)->index();
			$table->char('codigoTipoComprobante', 3)->index();
			$table->string('reclamoNumero', 20)->nullable();
			$table->text('reclamoDetalle')->nullable();
			$table->dateTime('reclamoFecha')->nullable();
			$table->string('clienteNombre', 250)->nullable();
			$table->string('clienteAppaterno', 250)->nullable();
			$table->string('clienteApmaterno', 250)->nullable();
			$table->string('clienteNumDoc', 20)->nullable();
			$table->string('clienteEmail', 45)->nullable();
			$table->text('clienteDireccion')->nullable();
			$table->integer('clienteDepartamento')->nullable();
			$table->integer('clienteProvincia')->nullable();
			$table->integer('clienteDistrito')->nullable();
			$table->string('clienteTelefono', 20)->nullable();
			$table->char('clienteEsMayorEdad', 1)->nullable();
			$table->text('clienteApoderado')->nullable();
			$table->integer('clienteMetodoNotificacion')->nullable();
			$table->dateTime('comprobanteFecha')->nullable();
			$table->string('comprobanteSerie', 10)->nullable();
			$table->string('comprobanteNumero', 10)->nullable();
			$table->string('productoCodigo', 20)->nullable();
			$table->decimal('productoPrecio', 10, 0)->nullable();
			$table->text('productoDescripcion')->nullable();
			$table->char('flagProcesado', 1)->nullable()->default(0);

            $table->foreign('codigoTipoBien')->references('codigoTipoBien')->on('tipo_bien');
            $table->foreign('codigoTipoDocumento')->references('codigoTipoDocumento')->on('tipo_documento');
            $table->foreign('codigoTipoComprobante')->references('codigoTipoComprobante')->on('tipo_comprobante');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reclamo');
	}

}
